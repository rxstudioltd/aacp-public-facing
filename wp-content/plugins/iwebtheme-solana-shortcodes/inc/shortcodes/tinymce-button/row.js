(function($){
	$('html').themeshortcode({
		id							: 'row',
		title 						: 'Insert row',			
		image						: 'row.png',
		showWindow					: true,	
		fields						: [ {type : 'label', name : 'Insert row inside container to divide content'}] 
	}, 
	function(ed, url, options) 
	{

	var html = '[row]<br />';
		html += 'your content here<br />';
		html += '[/row]';
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);