(function($){
	$('html').themeshortcode({
		id							: 'pricingbg',
		title 						: 'Insert pricing table background - wrapper',			
		image						: 'pricingwrap.png',
		showWindow					: true,	
		fields						: [ {type : 'label', name : 'Insert pricing table gray background, put this before insert pricing table style 2'}] 
	}, 
	function(ed, url, options) 
	{

	var html = '[pricingbg]<br />';
		html += 'pricing table style 2 here<br />';
		html += '[/pricingbg]';
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);