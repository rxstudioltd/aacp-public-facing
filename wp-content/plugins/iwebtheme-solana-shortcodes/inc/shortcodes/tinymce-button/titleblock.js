(function($){
	$('html').themeshortcode({
		id							: 'titleblock',
		title 						: 'Insert block title',			
		image						: 'subheader.png',
		showWindow					: true,
		fields						: [ { type : 'select' , name : 'Block title font size'	, id: 'select-size', option : ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']},

										{ type : 'text' , name : 'Block title text' , id: 'txt'}] 
	}, 
	function(ed, url, options) 
	{
		var size 	= $('#'+options.pluginprefix + 'select-size').val();
		var txt 	= $('#'+options.pluginprefix + 'txt').val();

		
		var html = '[titleblock size="'+size+'"]' + txt + '[/titleblock]';		
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);