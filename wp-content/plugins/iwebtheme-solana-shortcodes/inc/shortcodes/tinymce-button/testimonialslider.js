(function($){
	$('html').themeshortcode({
			id							: 'testimonialslider',
			title 						: 'Insert Testimonial Slider',			
			image						: 'discussion.png',
			showWindow					: true,	
			fields						: [ {type : 'label',      name : 'Insert '	, id: 'info'},
											{type : 'text'  , name : 'Testimonial slider title'    , id: 'testitle'},
											{type : 'text'  , name : 'Testimonial numbers in slide (number only)'    , id: 'tescount'},
											{type : 'select', name : 'Select column size ', id : 'select-column' , option : ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'one-third', 'two-thirds', 'in-one-third','in-two-thirds','in-sixteen','in-eight','in-four','in-three-four','block-3-col','block-4-col']} ,
										{type : 'select'  , name : 'Set column type'     , id: 'select-coltype' , option : ['columns', 'in-columns','column', 'in-column','block-column']}]
		}, 
		function(ed, url, options) 
		{
			var title 			= $('#'+options.pluginprefix + 'testitle').val();
			var count 			= $('#'+options.pluginprefix + 'tescount').val();
			var size 	= $('#'+options.pluginprefix + 'select-column').val();
			var ctype 	= $('#'+options.pluginprefix + 'select-coltype').val();

			
			var html = '[testimonialslider title="'+title+'" count="'+count+'" size="'+size+' '+ctype+' alpha"]';			
			ed.execCommand('mceInsertContent', false, html);
		});
})(jQuery);