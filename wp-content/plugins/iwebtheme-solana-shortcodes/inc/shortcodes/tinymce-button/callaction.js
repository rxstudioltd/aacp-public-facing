(function($){
	$('html').themeshortcode({
		id							: 'callaction',
		title 						: 'Insert call action box',			
		image						: 'callaction.png',
		showWindow					: true,	
		fields						: [ {type : 'text'  , name : 'Call action title'     	, id: 'title'},
										{type : 'text'  , name : 'Call action text'     , id: 'text'}, 		
										{type : 'text'  , name : 'Button text'     		, id: 'btntxt'},
										{type : 'text'  , name : 'Button link url'     		, id: 'url'} ]
	}, 
	function(ed, url, options) 
	{
		var title		= $('#'+options.pluginprefix + 'title').val();
		var text		= $('#'+options.pluginprefix + 'text').val();
		var url		= $('#'+options.pluginprefix + 'url').val();
		var btntxt		= $('#'+options.pluginprefix + 'btntxt').val();

		
		var html = '[callaction title="'+ title +'" text="'+ text +'" buttontext="'+ btntxt +'" url="'+ url +'"]';
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);