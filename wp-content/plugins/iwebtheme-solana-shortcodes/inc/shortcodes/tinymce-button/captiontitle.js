(function($){
	$('html').themeshortcode({
		id							: 'captiontitle',
		title 						: 'Insert caption title',			
		image						: 'styledtitle.png',
		showWindow					: true,
		fields						: [	{ type : 'text' , name : 'Styled title text' , id: 'txt'}] 
	}, 
	function(ed, url, options) 
	{
		var txt 	= $('#'+options.pluginprefix + 'txt').val();

		
		var html = '[captiontitle]' + txt + '[/captiontitle]';		
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);