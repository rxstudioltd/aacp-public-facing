(function($){
	$('html').themeshortcode({
		id							: 'container',
		title 						: 'Insert content container',			
		image						: 'container.png',
		showWindow					: true,
		windowWidth					: 800,
		fields						: [ 					
										{type : 'select'  , name : 'Add additional container margin bottom'     , id: 'select-mbot' , option : ['0','15px', '20px', '25px', '30px', '35px', '50px', '135px']},
										{type : 'select'  , name : 'Add additional container margin top'     , id: 'select-mtop' , option : ['-75px','-58px', '-1px', '0', '60px', '80px', '100px']},
										{type : 'select'  , name : 'Add container padding'     , id: 'select-pad' , option : ['0','top 15px', 'top-bottom 30px']},
										{type : 'select'  , name : 'Add clearfix'     , id: 'select-clear' , option : ['no','clearfix']}
										] 
	}, 
	function(ed, url, options) 
	{	
		var conmbot 	= $('#'+options.pluginprefix + 'select-mbot').val();
		var conmtop 	= $('#'+options.pluginprefix + 'select-mtop').val();
		var pad 	= $('#'+options.pluginprefix + 'select-pad').val();
		var conclear 	= $('#'+options.pluginprefix + 'select-clear').val();
		
	
		
		var mtopclass = '';
		if(conmtop == "-75px") {
			mtopclass = 'min-m-top-75';
		} else if(conmtop == "-58px") {
			mtopclass = 'min-m-top-58';
		} else if(conmtop == "-1px") {
			mtopclass = 'm-t-min-1';
		} else if(conmtop == "0") {
			mtopclass = '0';
		} else if(conmtop == "60px") {
			mtopclass = 'm-top-60';
		} else if(conmtop == "80px") {
			mtopclass = 'm-top-80';
		} else if(conmtop == "100px") {
			mtopclass = 'm-top-100';
		} 

	
		var mbotclass = '';
		if(conmbot == "0") {
			mbotclass = 'no';
		} else if(conmbot == "15px") {
			mbotclass = 'm-bot-15';
		} else if(conmbot == "20px") {
			mbotclass = 'm-bot-20';
		} else if(conmbot == "25px") {
			mbotclass = 'm-bot-25';
		} else if(conmbot == "30px") {
			mbotclass = 'm-bot-30';
		} else if(conmbot == "35px") {
			mbotclass = 'm-bot-35';
		} else if(conmbot == "50px") {
			mbotclass = 'm-bot-50';
		} else if(conmbot == "135px") {
			mbotclass = 'm-bot-135';
		}


var html = '[container marginbottom="'+ mbotclass +'" margintop="'+ mtopclass +'" padding="'+ pad +'" clear="'+ conclear +'"]<br />';
	html += 'your content here';
	html += '<br />[/container]';

		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);