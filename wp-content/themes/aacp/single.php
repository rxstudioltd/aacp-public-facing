<?php
/**
 * @package WordPress
 */
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
?>
<?php get_header(); ?>
<?php
$post_title = get_post_meta($post->ID, 'iweb_post_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.grey-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp.jpg);
		height:350px;
		background-color: transparent !important;
		background-position: 50% 80%;
		margin-bottom: 30px;
	}	
</style>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<h1 class="page-title"><?php the_title(); ?></h1>
			
			</div>	
		</div>
	</div>	
</div>	<!-- Grey bg end -->
<div class="container clearfix">

	<div class="sixteen columns m-bot-25">
			<?php get_template_part( 'loop' , 'content');?>			
	</div>	

</div>     

<?php if($mb_signup != 'Disable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>

<?php get_footer(); ?>