<?php
/*
Template Name: Blog page
*/
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.grey-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp.jpg);
		height:550px;
		background-color: transparent !important;
		background-position: 50% 80%;
	}	
</style>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<h1 class="page-title"><?php echo $page_title; ?></h1>
			</div>	
		</div>
	</div>	
</div>	<!-- Grey bg end -->

<div class="container clearfix">

		
		<div class="sixteen columns m-bot-25">
		<?php query_posts('post_type=post&post_status=publish&paged='. get_query_var('paged')); ?>
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?> 
			<?php setPostViews(get_the_ID()); ?>			
			<?php get_template_part( 'includes/content', get_post_format() ); ?>
					<?php endwhile; ?>

			<?php if (function_exists("pagination")) { ?>
			<div class="pagination-1-container">
			<?php pagination(); ?>
			</div>
			<?php } else {
			posts_nav_link(' &#183; ', 'previous page', 'next page'); 	
			} ?>
		
		<?php endif; ?>
		<?php wp_reset_query(); ?>
		</div>	
	

</div>
<!-- end of section -->

<?php if($mb_signup != 'Disable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>

<?php get_footer(); ?>