<?php
/*
Template Name: Fullwidth page
*/
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.grey-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp.jpg);
		height:550px;
		background-color: transparent !important;
		background-position: 50% 80%;
	}	
</style>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
			<?php if ($page_title != '') { ?>
				<h2 class="page-title"><?php echo $page_title; ?></h2>
			<?php } else { ?>
				<?php get_template_part('includes/breadcrumbs'); ?>	
			<?php } ?>
			</div>	
		</div>
	</div>	
<?php get_template_part('includes/part-custom-top'); ?>
</div>	<!-- Grey bg end -->

<div class="container m-bot-35 clearfix">

		<div class="sixteen columns">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?> 
			<?php the_content(); ?>
			<?php endwhile; ?>

		<?php endif; ?>
		
		</div>	

</div>
<?php get_template_part('includes/part-custom-bottom'); ?>
<?php if($mb_portfolio == 'Enable') { ?>
	<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php } ?>
<?php if($mb_signup == 'Enable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>
<?php if($mb_clients == 'Enable') { ?>
	<?php get_template_part( 'includes/part-clients' ); ?>
<?php } ?>	
<?php get_footer(); ?>