<?php
/*
Template Name: Homepage 3
*/
get_header();
$homepage = 'Homepage 3';
?>
<?php
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<?php get_template_part('includes/slider-sequence'); ?>
<?php get_template_part('includes/part-custom-top'); ?>
</div>	<!-- Grey bg end -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>	
<div class="container clearfix">
	<div class="sixteen columns">	
		
		<?php the_content(); ?>
		
	</div>
</div>
<?php endwhile; endif; ?>	
<?php get_template_part('includes/part-custom-bottom'); ?>
<!-- end of section -->
<?php if($mb_portfolio == 'Enable') { ?>
	<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php } ?>
<?php if($mb_signup == 'Enable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>
<?php if($mb_clients == 'Enable') { ?>
	<?php get_template_part( 'includes/part-clients' ); ?>
<?php } ?>
<?php get_footer(); ?>