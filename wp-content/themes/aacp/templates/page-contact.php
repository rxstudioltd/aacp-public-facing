<?php
/*
Template Name: Contact page
*/
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<!-- PAGE TITLE -->
	<div class="container clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
			<?php if ($page_title != '') { ?>
				<h1 class="page-title"><?php echo $page_title; ?></h1>
			<?php } else { ?>
				<?php get_template_part('includes/breadcrumbs'); ?>	
			<?php } ?>
			</div>	
		</div>
	</div>	
</div>	<!-- Grey bg end -->
	<div class="clearfix">
		<div class="m-bot-10">
		<?php if(iwebtheme_smof_data('map_enable') != 0) { ?>
				<!-- Google Maps -->
					<section class="google-map-container">
						<div id="googlemaps" class="google-map"></div>
					</section>
				<!-- Google Maps / End -->
		<?php } ?>
		</div>
	</div>

<?php
get_template_part('includes/contact-function'); 
?>	
<!-- CONTACT FORM-->
<div class="container clearfix">
		<div class="twelve columns  m-bot-35">
			<div class="caption-container-main m-bot-30">
				<div class="caption-text-container"><?php echo __('<span class="bold">SEND</span> US A MESSAGE','iwebtheme'); ?></div>
				<div class="content-container-white caption-bg "></div>
			</div>
		
		
			<div class="contact-form-container">
				<form action="<?php the_permalink(); ?>" id="contact-form" method="post" class="clearfix">			
					<fieldset class="field-1-3 left">
						<label><?php echo __('Name','iwebtheme'); ?></label>
						<input type="text" name="contactName" id="contactName" onblur="if(this.value=='')this.value='<?php echo __('Your name...','iwebtheme'); ?>';" onfocus="if(this.value=='<?php echo __('Your name...','iwebtheme'); ?>')this.value='';" value="<?php echo __('Your name...','iwebtheme'); ?>" class="text requiredField m-bot-20" >
					</fieldset >
					<fieldset class="field-1-3 left">
						<label><?php echo __('Email','iwebtheme'); ?></label>	
						<input type="text" name="contactEmail" id="contactEmail" onblur="if(this.value=='')this.value='<?php echo __('Your email...','iwebtheme'); ?>';" onfocus="if(this.value=='<?php echo __('Your email...','iwebtheme'); ?>')this.value='';" value="<?php echo __('Your email...','iwebtheme'); ?>" class="text requiredField email m-bot-20" >
					</fieldset>
					<fieldset class="field-1-3 left">
						<label><?php echo __('Subject','iwebtheme'); ?></label>	
						<input type="text" name="subject" id="subject" onblur="if(this.value=='')this.value='<?php echo __('Subject...','iwebtheme'); ?>';" onfocus="if(this.value=='Subject...')this.value='';" value="<?php echo __('Subject...','iwebtheme'); ?>" class="text requiredField subject m-bot-20" >
					</fieldset>	
					<fieldset class="field-1-1 left">
						<label><?php echo __('Message','iwebtheme'); ?></label>
						<textarea name="comments" id="comments" rows="5" cols="30" class="text requiredField" onblur="if(this.value=='')this.value='<?php echo __('Your message...','iwebtheme'); ?>';" onfocus="if(this.value=='<?php echo __('Your message...','iwebtheme'); ?>')this.value='';" ><?php echo __('Your message...','iwebtheme'); ?></textarea>
					</fieldset>
					<fieldset class="right m-t-min-1">
						<input name="Mysubmitted" id="Mysubmitted" value="<?php echo __('SEND','iwebtheme'); ?>" class="button medium" type="submit" >
						<input type="hidden" name="submitted" id="submitted" value="true" />
						<input type="hidden" name="contact_success" id="contact_success" value="<?php echo iwebtheme_smof_data('contact_success');?>" />
					</fieldset>
				</form>
			</div>
		</div>
		

		<!-- SIDEBAR -->
		<div class="four columns  m-bot-25">
			
			<div class="caption-container-main m-bot-30">
				<div class="caption-text-container"><?php echo __('<span class="bold">CONTACT</span> INFO','iwebtheme'); ?></div>
				<div class="content-container-white caption-bg "></div>
			</div>
			
			<div class="">
					<ul class="contact-list">
						<?php if(iwebtheme_smof_data('cinfo_address') != '') { ?>
						<li class="contact-loc">						
							<?php echo iwebtheme_smof_data('cinfo_address'); ?>					
						</li>
						<?php } ?>
						<?php if(iwebtheme_smof_data('cinfo_phone') != '') { ?>
						<li class="contact-phone">
							<?php echo iwebtheme_smof_data('cinfo_phone'); ?>			
						</li>
						<?php } ?>
						<?php if(iwebtheme_smof_data('cinfo_email') != '') { ?>
						<li class="contact-mail">
							<?php echo iwebtheme_smof_data('cinfo_email'); ?>	
						</li>
						<?php } ?>
					</ul>				
			</div>		
		</div>
		
</div>	
<?php if($mb_portfolio != 'Disable') { ?>
	<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php } ?>
<?php if($mb_signup != 'Disable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>
<?php if($mb_clients != 'Disable') { ?>
	<?php get_template_part( 'includes/part-clients' ); ?>
<?php } ?>	
<?php get_footer(); ?>