<?php
/*
Template Name: Portfolio 2 columns
*/
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
			<?php if ($page_title != '') { ?>
				<h1 class="page-title"><?php echo $page_title; ?></h1>
			<?php } else { ?>
				<?php get_template_part('includes/breadcrumbs'); ?>	
			<?php } ?>
				<?php
					$taxonomy = 'portfolio_categories';
					$categories = get_terms( $taxonomy, array( 'parent' => 0, ) );
					if ( count($categories) > 0 ){
						echo '<ul id="filter">';
						echo '<li class="current all"><a href="#">'.__('All', 'iwebtheme').'</a></li>';
						foreach ( $categories as $category ) {
							$catasclass = $category->name;
							$catasclass = preg_replace('/\s/', '', $catasclass);
							echo '<li class="'.$catasclass.'"><a href="#">'.$category->name.'</a></li>';
						}
						echo '</ul>';
					}					
				?>
			</div>	
		</div>
	</div>	
</div>	<!-- Grey bg end -->	
<!-- CONTENT -->	
<div class="container filter-portfolio clearfix">
	<ul id="portfolio" class="clearfix">
			<?php if(iwebtheme_smof_data('disable_portpagination') != 0) { 	
					$port_count = iwebtheme_smof_data('port_count');
					$paged = (get_query_var('paged') ? get_query_var('paged') : 1);
                    $count = 1;
        		    $type = 'portfolio';
        		    $args=array(
        		    'post_type' => $type,
					'paged' => $paged,
                    'posts_per_page' => $port_count
        		    );
        		    query_posts($args);	
				} else { 
 					$port_count = iwebtheme_smof_data('port_count');
				
                    $count = 1;
        		    $type = 'portfolio';
        		    $args=array(
        		    'post_type' => $type,
                    'posts_per_page' => -1
        		    );
        		    query_posts($args);	
				} ?>

					<?php if (have_posts()) : while (have_posts()) : the_post();					
					$terms = ''; // variable
					
					if (has_post_thumbnail()) {					
						$thumb = get_post_thumbnail_id();
						$thumb_w = '460';
						$thumb_h = '272';
						$image_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
						$image_url = $image_src [0];
						$attachment_url = wp_get_attachment_url($thumb, 'full');
						$image = aq_resize($attachment_url, $thumb_w, $thumb_h, true);							
					}			
        			
					
					$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'grid-thumb');
					$terms = get_the_terms( $post->ID, 'portfolio_categories' );
									foreach ( $terms as $term ) {
											$cats[0] = $term->name;
											$catname = join($cats);		
											$catname = preg_replace('/\s/', '', $catname);											
									}
					$title= get_the_title();
					$title= explode(' ',$title);
					$title[0]= '<span class="bold">'.$title[0].'</span>';
					$title= implode(' ',$title);
                    ?>	
	
		<!-- PORTFOLIO ITEM -->
			<li data-id="id-<?php echo $post->ID; ?>" data-type="<?php 
						$terms = get_the_term_list( $post->ID, 'portfolio_categories','',' , ','' ); 
						$terms = preg_replace('/\s/','', $terms);
						$terms = strip_tags( $terms );
						$terms = preg_replace('/[\s,\-!]/',' ', $terms);
						echo $terms;
						?>" class="eight columns m-bot-35">
						<div class="hover-item">
							<div class="view view-first">
								<img src="<?php echo $image; ?>" alt="<?php the_title(); ?>" />
								<div class="mask"></div>
								<div class="abs">
									<a href="<?php echo $attachment_url; ?>" class="lightbox zoom info"></a><a href="<?php the_permalink(); ?>" class="link info"></a>
								</div>	
							</div>
							<div class="lw-item-caption-container">
								<a class="a-invert" href="<?php the_permalink(); ?>" >
								<div class="item-title-main-container clearfix">
										<div class="item-title-text-container">
								<?php echo $title; ?>
								</div></div>
								</a>
								<div class="item-caption"><?php 
						$terms = get_the_term_list( $post->ID, 'portfolio_categories','',' , ','' ); 
						$terms = preg_replace('/\s/','', $terms);
						$terms = strip_tags( $terms );
						$terms = preg_replace('/[\s,\-!]/',' ', $terms);
						echo $terms.' ';
						?></div>
							</div>
						</div>
			</li>
  <?php endwhile; ?>	
	</ul>

</div>

<?php if(iwebtheme_smof_data('disable_portpagination') != 0) { ?>
	<div class="container m-bot-35 clearfix">
			<?php if (function_exists("pagination")) { ?>
			<div class="pagination-1-container sixteen columns">
			<?php pagination(); ?>
			</div>
			<?php } else {
			posts_nav_link(' &#183; ', 'previous page', 'next page'); 	
			} ?>
	</div>
<?php } ?>
<?php endif; wp_reset_query(); ?>		
<?php if($mb_portfolio != 'Disable') { ?>
	<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php } ?>
<?php if($mb_signup != 'Disable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>
<?php if($mb_clients != 'Disable') { ?>
	<?php get_template_part( 'includes/part-clients' ); ?>
<?php } ?>	
<!-- end of section -->
<?php get_footer(); ?>