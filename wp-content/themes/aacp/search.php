<?php
/**
 * @package WordPress
 * @subpackage Solana Theme
 */
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
?>
<?php get_header(); ?>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<h1 class="page-title"><?php echo __('Search Results For','iwebtheme'); ?>: <?php the_search_query(); ?></h1>
			</div>	
		</div>
	</div>	
</div>	<!-- Grey bg end -->

<div class="container m-bot-35 clearfix">

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	query_posts($query_string .'&paged=' . $paged);
?>
	<?php if ($sidebar_pos == 'left') { ?>
		<?php get_sidebar(); ?>
	<?php } ?>
	
	<div class="eleven columns m-bot-25">
	<?php
    if (have_posts()) : ?>        
  
			<?php
            while (have_posts()) : the_post(); ?>        
                <div class="blog-item m-bot-35 clearfix">
                    <?php
                    $portfolio_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                    if($portfolio_thumb) { ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="search-portfolio-thumb"><img src="<?php echo $portfolio_thumb[0]; ?>" height="<?php echo $portfolio_thumb[2]; ?>" width="<?php echo $portfolio_thumb[1]; ?>" alt="<?php echo the_title(); ?>" style="margin-bottom:30px;" /></a>
                    <?php } ?>
                    <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt('50'); ?>
                </div>
                <!-- /search-entry -->                
            <?php endwhile; ?>            
			<?php if (function_exists("pagination")) { ?>
			<div class="pagination-1-container">
			<?php pagination(); ?>
			</div>
			<?php } else {
			posts_nav_link(' &#183; ', 'previous page', 'next page'); 	
			} ?>
		<?php else : ?>        
		<div class="blog-item m-bot-35 clearfix">
			<?php _e('No results found for that query.', 'iwebtheme'); ?>
		</div>
        <!-- /post  -->   

 		
		<?php endif; ?>
		</div>
		
	<?php if ($sidebar_pos == 'right') { ?>
	<?php get_sidebar(); ?>
	<?php } ?>

</div>
<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php get_template_part( 'includes/part-clients' ); ?>
<?php get_footer(); ?>