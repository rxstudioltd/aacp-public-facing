<?php
/*============================================
Custom post type
=============================================*/

// register homepage sections cpt
add_action( 'init', 'iwebtheme_post_type_customsections' );

function iwebtheme_post_type_customsections() {

    $labels = array( 
        'name' => _x( 'Custom sections', 'customsections', 'iwebtheme' ),
        'singular_name' => _x( 'Custom section', 'customsections', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'customsections', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Custom section', 'customsections', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Custom section', 'customsections', 'iwebtheme' ),
        'new_item' => _x( 'New Custom section', 'customsections', 'iwebtheme' ),
        'view_item' => _x( 'View Custom section', 'customsections', 'iwebtheme' ),
        'search_items' => _x( 'Search Custom section', 'customsections', 'iwebtheme' ),
        'not_found' => _x( 'No Custom section found', 'customsections', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No Custom section found in Trash', 'customsections', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Custom section:', 'customsections', 'iwebtheme' ),
        'menu_name' => _x( 'Custom sections', 'customsections', 'iwebtheme' ),
		'all_items' => _x( 'All Custom sections', 'customsections', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'editor' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
		'menu_icon' => get_template_directory_uri() . '/images/admin/ui-custom.png'
    );

    register_post_type( 'customsections', $args );
}


// register sequenceslider cpt
/*add_action( 'init', 'iwebtheme_post_type_sequenceslider' );

function iwebtheme_post_type_sequenceslider() {

    $labels = array( 
        'name' => _x( 'Sequencesliders', 'sequenceslider', 'iwebtheme' ),
        'singular_name' => _x( 'Sequenceslider', 'sequenceslider', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'sequenceslider', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Sequence slider', 'sequenceslider', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Sequence slider', 'sequenceslider', 'iwebtheme' ),
        'new_item' => _x( 'New Sequence slider', 'sequenceslider', 'iwebtheme' ),
        'view_item' => _x( 'View Sequence slider', 'sequenceslider', 'iwebtheme' ),
        'search_items' => _x( 'Search Sequence slider', 'sequenceslider', 'iwebtheme' ),
        'not_found' => _x( 'No sequence slider found', 'sequenceslider', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No sequence slider found in Trash', 'sequenceslider', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Sequence slider:', 'sequenceslider', 'iwebtheme' ),
        'menu_name' => _x( 'Sequence sliders', 'sequenceslider', 'iwebtheme' ),
		'all_items' => _x( 'All Sequence sliders', 'sequenceslider', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
		'menu_icon' => get_template_directory_uri() . '/images/admin/icon-slider.png',
        'capability_type' => 'post'
    );

    register_post_type( 'sequenceslider', $args );
}*/

// columns for sequenceslider custom post
/*add_filter( 'manage_edit-sequenceslider_columns', 'iweb_edit_sequenceslider_columns' ) ;
function iweb_edit_sequenceslider_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Slider item', 'iwebtheme' ),
		'template' => __( 'Used for (page template name)', 'iwebtheme' ),
		'date' => __( 'Date', 'iwebtheme' )
	);

	return $columns;
}*/

//add_action( 'manage_sequenceslider_posts_custom_column', 'iweb_manage_sequenceslider_columns', 10, 2 );

//function iweb_manage_sequenceslider_columns( $column, $post_id ) {
	//global $post;

	//switch( $column ) {
		/* If displaying the 'group' column. */
		//case 'template' :

			/* Get the post meta. */
			//$template = get_post_meta($post->ID, 'iweb_seq_template', TRUE);
			/* If no template is found, output a default message. */
			//printf( $template );
			//break;
		/* Just break out of the switch statement for everything else. */
		//default :
			//break;
	//}
//}

/*add_filter( 'manage_edit-sequenceslider_sortable_columns', 'iweb_sequenceslider_sortable_columns' );

function iweb_sequenceslider_sortable_columns( $columns ) {

	$columns['template'] = 'template';

	return $columns;
}*/


 
/* ================================= end sequnce slider ================================*/

// register flexslider cpt
add_action( 'init', 'iwebtheme_post_type_flexslider' );

function iwebtheme_post_type_flexslider() {

    $labels = array( 
        'name' => _x( 'Flexsliders', 'flexslider', 'iwebtheme' ),
        'singular_name' => _x( 'Flexslider', 'flexslider', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'flexslider', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Flexslider slider', 'flexslider', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Flexslider slider', 'flexslider', 'iwebtheme' ),
        'new_item' => _x( 'New Flexslider slider', 'flexslider', 'iwebtheme' ),
        'view_item' => _x( 'View Flexslider slider', 'flexslider', 'iwebtheme' ),
        'search_items' => _x( 'Search Flexslider slider', 'flexslider', 'iwebtheme' ),
        'not_found' => _x( 'No flexslider slider found', 'flexslider', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No flexslider slider found in Trash', 'flexslider', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Flexslider slider:', 'flexslider', 'iwebtheme' ),
        'menu_name' => _x( 'Flexslider sliders', 'flexslider', 'iwebtheme' ),
		'all_items' => _x( 'All Flexslider sliders', 'flexslider', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
		'menu_icon' => get_template_directory_uri() . '/images/admin/icon-slider.png',
        'capability_type' => 'post'
    );

    register_post_type( 'flexslider', $args );
}


// columns for sequenceslider custom post
/*add_filter( 'manage_edit-flexslider_columns', 'iweb_edit_flexslider_columns' ) ;
function iweb_edit_flexslider_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Slider item', 'iwebtheme' ),
		'template' => __( 'Used for (page template name)', 'iwebtheme' ),
		'date' => __( 'Date', 'iwebtheme' )
	);

	return $columns;
}

add_action( 'manage_flexslider_posts_custom_column', 'iweb_manage_flexslider_columns', 10, 2 );

function iweb_manage_flexslider_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {*/
		/* If displaying the 'group' column. */
		/*case 'template' :*/

			/* Get the post meta. */
			//$template = get_post_meta($post->ID, 'iweb_flexslider_template', TRUE);
			/* If no template is found, output a default message. */
			//printf( $template );
			//break;
		/* Just break out of the switch statement for everything else. */
		//default :
			//break;
	//}
//}

/*add_filter( 'manage_edit-flexslider_sortable_columns', 'iweb_flexslider_sortable_columns' );

function iweb_flexslider_sortable_columns( $columns ) {

	$columns['template'] = 'template';

	return $columns;
}*/
 
/* ================================= end flexslider ================================*/


// register testimonial cpt
//add_action( 'init', 'iwebtheme_post_type_testimonial' );

/*function iwebtheme_post_type_testimonial() {

    $labels = array( 
        'name' => _x( 'Testimonials', 'testimonial', 'iwebtheme' ),
        'singular_name' => _x( 'Testimonial', 'testimonial', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'testimonial', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Testimonial', 'testimonial', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Testimonial', 'testimonial', 'iwebtheme' ),
        'new_item' => _x( 'New Testimonial', 'testimonial', 'iwebtheme' ),
        'view_item' => _x( 'View Testimonial', 'testimonial', 'iwebtheme' ),
        'search_items' => _x( 'Search Testimonial', 'testimonial', 'iwebtheme' ),
        'not_found' => _x( 'No testimonial found', 'testimonial', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No testimonial found in Trash', 'testimonial', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Testimonial:', 'testimonial', 'iwebtheme' ),
        'menu_name' => _x( 'Testimonials', 'testimonial', 'iwebtheme' ),
		'all_items' => _x( 'All Testimonials', 'testimonial', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
		'menu_icon' => get_template_directory_uri() . '/images/admin/icon-testimonial.png',
        'capability_type' => 'post'
    );

    register_post_type( 'testimonial', $args );
}*/

/* ================================= end testimonial ================================*/



// register client cpt
/*add_action( 'init', 'iwebtheme_post_type_client' );

function iwebtheme_post_type_client() {

    $labels = array( 
        'name' => _x( 'Clients', 'client', 'iwebtheme' ),
        'singular_name' => _x( 'Client', 'client', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'client', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Client', 'client', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Client', 'client', 'iwebtheme' ),
        'new_item' => _x( 'New Client', 'client', 'iwebtheme' ),
        'view_item' => _x( 'View Client', 'client', 'iwebtheme' ),
        'search_items' => _x( 'Search Client', 'client', 'iwebtheme' ),
        'not_found' => _x( 'No Client found', 'client', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No client found in Trash', 'client', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Client:', 'client', 'iwebtheme' ),
        'menu_name' => _x( 'Clients', 'client', 'iwebtheme' ),
		'all_items' => _x( 'All Clients', 'client', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
		'menu_icon' => get_template_directory_uri() . '/images/admin/icon-client.png',
        'capability_type' => 'post'
    );

    register_post_type( 'client', $args );
}*/

/* ================================= end client ================================*/


// register portfolio cpt

/*add_action( 'init', 'iwebtheme_post_type_portfolio' );

function iwebtheme_post_type_portfolio() {

    $labels = array( 
        'name' => _x( 'Portfolios', 'portfolio', 'iwebtheme' ),
        'singular_name' => _x( 'Portfolio', 'portfolio', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'portfolio', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Portfolio', 'portfolio', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Portfolio', 'portfolio', 'iwebtheme' ),
        'new_item' => _x( 'New Portfolio', 'portfolio', 'iwebtheme' ),
        'view_item' => _x( 'View Portfolio', 'portfolio', 'iwebtheme' ),
        'search_items' => _x( 'Search Portfolio', 'portfolio', 'iwebtheme' ),
        'not_found' => _x( 'No portfolio found', 'portfolio', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No portfolio found in Trash', 'portfolio', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Portfolio:', 'portfolio', 'iwebtheme' ),
        'menu_name' => _x( 'Portfolios', 'portfolio', 'iwebtheme' ),
		'all_items' => _x( 'All Portfolios', 'portfolio', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
		'menu_icon' => get_template_directory_uri() . '/images/admin/icon-portfolio.png',
        'capability_type' => 'post'
    );

    register_post_type( 'portfolio', $args );
}*/

// add taxonomies to categorize different custom post types

// portfolio tax
/*add_action( 'init', 'build_taxonomies', 0);
function build_taxonomies() {
register_taxonomy("portfolio_categories", array("portfolio"), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => true));
}

add_filter("manage_edit-portfolio_columns", "add_new_columns");  
add_action("manage_posts_custom_column",  "add_column_data", 2,10 );

function add_new_columns($defaults) {
    $defaults['portfolio_categories'] = __('Categories', 'iwebtheme');
    return $defaults;
}
function add_column_data( $column_name, $post_id ) {
	if( $column_name == 'portfolio_categories' ) {
		$_taxonomy = 'portfolio_categories';
		$terms = get_the_terms( $post_id, $_taxonomy );
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $c )
				$out[] = "<a href='edit-tags.php?action=edit&taxonomy=$_taxonomy&post_type=portfolio&tag_ID={$c->term_id}'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'category', 'display')) . "</a>";
			echo join( ', ', $out );
		}
		else {
			_e('No Category', 'iwebtheme');
		}
	}
}*/
?>