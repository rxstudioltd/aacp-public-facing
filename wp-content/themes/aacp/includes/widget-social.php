<?php
// Add function to widgets_init
add_action( 'widgets_init', 'iwebtheme_social_widgets' );
add_filter('iwebtheme_social_widgets', 'do_shortcode');
// Register widget
function iwebtheme_social_widgets() {
	register_widget( 'iwebtheme_sociaL_Widget' );
}
// Widget class
class iwebtheme_social_widget extends WP_Widget {
	
function iwebtheme_sociaL_Widget() {

	// Widget settings
	$widget_ops = array(
		'classname' => 'iwebtheme_social_widget',
		'description' => __('Display social icon.', 'iwebtheme')
	);

	// Widget control settings
	$control_ops = array(
		'width' => 300,
		'height' => 350,
		'id_base' => 'iwebtheme_social_widget'
	);

	// Create the widget
	$this->WP_Widget( 'iwebtheme_social_widget', __('Solana Social widget', 'iwebtheme'), $widget_ops, $control_ops );
	
}
	
function widget( $args, $instance ) {
	extract( $args );

	// Our variables from the widget settings
	$title = apply_filters('widget_title', $instance['title'] );
	$wtext	= apply_filters( 'wtext', $instance['wtext'] );
	
	$facebook = $instance['facebook'];
	$twitter = $instance['twitter'];
	$skype = $instance['skype'];
	$flickr = $instance['flickr'];
	$vimeo = $instance['vimeo'];
	$linkedin = $instance['linkedin'];
	$pintrest = $instance['pintrest'];
	$googleplus = $instance['googleplus'];

	// Before widget (defined by theme functions file)
	echo $before_widget;

	// Display the widget title if one was input
		$title = explode(' ', $title);
		if (count($title) > 1 ) {
		  $title[count($title)-1] = '<span class="bold">'.($title[count($title)-1]).'</span>';
		  $title = implode(' ', $title); 
		}

	// Display contact info
	 ?>
	<div class="footer-social-text-container">
	<p class="title-font-24"><?php echo $title; ?></p>
	<p class="title-font-12"><?php echo $wtext; ?></p>
	</div>
	<div class="footer-social-links-container">	
		<ul class="social-links clearfix">
		<?php if ($facebook !='') { ?>
		<li>
			<a class="facebook-link" target="_blank" title="Facebook" href="<?php echo $facebook; ?>"></a>
		</li>
		<?php } ?>
		<?php if ($skype !='') { ?>
		<li>
			<a class="skype-link" target="_blank" title="skype" href="<?php echo $skype; ?>"></a>
		</li>
		<?php } ?>
		<?php if ($twitter !='') { ?>
		<li>
			<a class="twitter-link" target="_blank" title="twitter" href="<?php echo $twitter; ?>"></a>
		</li>
		<?php } ?>
		<?php if ($flickr !='') { ?>
		<li>
			<a class="flickr-link" target="_blank" title="flickr" href="<?php echo $flickr; ?>"></a>
		</li>
		<?php } ?>
		<?php if ($vimeo !='') { ?>
		<li>
			<a class="vimeo-link" target="_blank" title="vimeo" href="<?php echo $vimeo; ?>"></a>
		</li>
		<?php } ?>
		<?php if ($linkedin !='') { ?>
		<li>
			<a class="linkedin-link" target="_blank" title="linkedin" href="<?php echo $linkedin; ?>"></a>
		</li>
		<?php } ?>
		<?php if ($pintrest !='') { ?>
		<li>
			<a class="pintrest-link" target="_blank" title="pintrest" href="<?php echo $pintrest; ?>"></a>
		</li>
		<?php } ?>
		<?php if ($googleplus !='') { ?>
		<li>
			<a class="googleplus-link" target="_blank" title="googleplus" href="<?php echo $googleplus; ?>"></a>
		</li>
		<?php } ?>
		</ul>
		</div>
			
	
	<?php
	// After widget (defined by theme functions file)
	echo $after_widget;	
}
	
function update( $new_instance, $old_instance ) {
	$instance = $old_instance;

	// Strip tags to remove HTML 
	$instance['title'] = strip_tags( $new_instance['title'] );
	$instance['wtext'] = strip_tags( $new_instance['wtext'] );
	
	$instance['facebook'] = strip_tags( $new_instance['facebook'] );
	$instance['skype'] = strip_tags( $new_instance['skype'] );
	$instance['twitter'] = strip_tags( $new_instance['twitter'] );
	$instance['flickr'] = strip_tags( $new_instance['flickr'] );
	$instance['vimeo'] = strip_tags( $new_instance['vimeo'] );
	$instance['linkedin'] = strip_tags( $new_instance['linkedin'] );
	$instance['pintrest'] = strip_tags( $new_instance['pintrest'] );
	$instance['googleplus'] = strip_tags( $new_instance['googleplus'] );
	return $instance;
}
	
	function form( $instance ) {
	
	// Set up some default widget settings
	$defaults = array(
			'title' => __( 'SAY HELLO', 'iwebtheme'), 
			'wtext' => __( 'WE\'D LOVE HEARING FROM YOU', 'iwebtheme'), 
			'facebook' => __( '#', 'iwebtheme' ),
			'skype' => __( '#', 'iwebtheme' ),
			'twitter' => __( '#', 'iwebtheme'), 
			'flickr' => __( '#', 'iwebtheme'), 
			'vimeo' => __( '#', 'iwebtheme'),
			'linkedin' => __( '#', 'iwebtheme'),
			'pintrest' => __( '#', 'iwebtheme'),
			'googleplus' => __( '#', 'iwebtheme')
	);

		$instance = wp_parse_args( (array)$instance, $defaults );
?>
	<p><label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php _e( 'Social widget Title', 'iwebtheme' ) ?></label>
    	<input type="text" class="widefat" name="<?php echo $this->get_field_name( 'title' ) ?>" id="<?php echo $this->get_field_id( 'title' ) ?>" value="<?php echo $instance['title'] ?>"/>
	</p>
	<p><label for="<?php echo $this->get_field_id( 'wtext' ) ?>"><?php _e( 'Social widget text', 'iwebtheme' ) ?></label>
    	<input type="text" class="widefat" name="<?php echo $this->get_field_name( 'wtext' ) ?>" id="<?php echo $this->get_field_id( 'wtext' ) ?>" value="<?php echo $instance['wtext'] ?>"/>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e('facebook:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="<?php echo $instance['facebook']; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'skype' ); ?>"><?php _e('skype:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'skype' ); ?>" name="<?php echo $this->get_field_name( 'skype' ); ?>" value="<?php echo $instance['skype']; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e('twitter:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php echo $instance['twitter']; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'flickr' ); ?>"><?php _e('flickr:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'flickr' ); ?>" name="<?php echo $this->get_field_name( 'flickr' ); ?>" value="<?php echo $instance['flickr']; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'vimeo' ); ?>"><?php _e('vimeo:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'vimeo' ); ?>" name="<?php echo $this->get_field_name( 'vimeo' ); ?>" value="<?php echo $instance['vimeo']; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'linkedin' ); ?>"><?php _e('linkedin:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'linkedin' ); ?>" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" value="<?php echo $instance['linkedin']; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'pintrest' ); ?>"><?php _e('pinterest:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'pintrest' ); ?>" name="<?php echo $this->get_field_name( 'pintrest' ); ?>" value="<?php echo $instance['pintrest']; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'googleplus' ); ?>"><?php _e('googleplus:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'googleplus' ); ?>" name="<?php echo $this->get_field_name( 'googleplus' ); ?>" value="<?php echo $instance['googleplus']; ?>" />
	</p>
<?php
	}
}
?>