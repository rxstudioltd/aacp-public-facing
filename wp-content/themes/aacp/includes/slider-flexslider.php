<?php 
global $post;
global $homepage;
$old = $post;	
?>
<?php					
    $count = 1;
	$type = 'flexslider';
    $args=array(
	    'post_type' => $type,
		'posts_per_page' => -1,
		'meta_query' => array(
				 array(
					'key' => 'iweb_flexslider_template',
					'value' => $homepage
				 )
			  )
         );
	$counter = 0;
$flex_query = new WP_Query( $args ); 
?>	
<?php if($homepage == 'Homepage 1') { ?>
<div class="slider-1 clearfix">

	<div class="flex-container">
		<div class="flexslider loading">
		
<?php } else if($homepage == 'Homepage 2') { ?>
<div id="slider-container" class="container clearfix">
	<div class="sixteen columns ">
		<div class="flexslider">
<?php } ?>

	<?php if($homepage == 'Homepage 1') { ?>
				<ul class="slides">
					<?php if($flex_query->have_posts()): 	
					while($flex_query->have_posts()) : $flex_query->the_post();					

					$title = get_post_meta($post->ID, 'iweb_flexslider_title', TRUE);
					$title= explode(' ',$title);
					$title[0]='<span class="bold">'.$title[0].'</span>';
					$title=implode(' ',$title);
					$flexcaption = get_post_meta($post->ID, 'iweb_flexslider_caption', TRUE);
					$bgimg = rwmb_meta( 'iweb_flexslider_imgbg', 'type=plupload_image' );
					$slideimg = '';
					foreach ( $bgimg as $image )
					{
					$bgimg = $image['full_url'];
					}
					$img = rwmb_meta( 'iweb_flexslider_img', 'type=plupload_image' );
					foreach ( $img as $image )
					{
					$slideimg = $image['full_url'];
					}
					$img_w = '902';
					$img_h = '390';
					//$img = aq_resize($img, $img_w, $img_h, true);
					$btntxt = get_post_meta($post->ID, 'iweb_flexslider_btntext', TRUE);
					$btnurl = get_post_meta($post->ID, 'iweb_flexslider_btnurl', TRUE);
                    ?>	

						<?php if ($bgimg !='') { ?>
						<li style="background:url(<?php echo $bgimg; ?>) no-repeat;background-position:50% 0">
						<?php } else { ?>
						<li>
						<?php } ?>
							<div class="container">
							 <div class="sixteen columns contain">
							 
								<p data-bottomtext="22%"><?php echo $flexcaption; ?></p>
									
							</div>
							</div>
						</li>
<?php $counter++ ; endwhile; endif;
$post = $old;
?>
				</ul>	
		<?php } ?>
	<?php if($homepage == 'Homepage 2') { ?>
				<ul class="slides">
					<?php if($flex_query->have_posts()): 	
					while($flex_query->have_posts()) : $flex_query->the_post();					

					$title = get_post_meta($post->ID, 'iweb_flexslider_title', TRUE);
					$title= explode(' ',$title);
					$title[0]='<span class="bold">'.$title[0].'</span>';
					$title=implode(' ',$title);
					$flexcaption = get_post_meta($post->ID, 'iweb_flexslider_caption', TRUE);
					$bgimg = rwmb_meta( 'iweb_flexslider_imgbg', 'type=plupload_image' );
					foreach ( $bgimg as $image )
					{
					$bgimg = $image['full_url'];
					}
					$img = rwmb_meta( 'iweb_flexslider_img', 'type=plupload_image' );
					foreach ( $img as $image )
					{
					$slideimg = $image['full_url'];
					}
					$img_w = '902';
					$img_h = '390';
					//$img = aq_resize($img, $img_w, $img_h, true);
					$btntxt = get_post_meta($post->ID, 'iweb_flexslider_btntext', TRUE);
					$btnurl = get_post_meta($post->ID, 'iweb_flexslider_btnurl', TRUE);
                    ?>	

						<li>
								<?php if ($slideimg !='') { ?>
								<img src="<?php echo $slideimg; ?>" alt="" class="item" data-topimage="21%" />
								<?php } ?>	
							<div class="flex-caption">

								<div class="title">
								<h2><?php echo $title; ?></h2>
								<p class="subtitle-2"><?php echo $flexcaption; ?></p>
								</div>
								<div class="subtitle-3">
								   <a class="button medium r-m-plus r-m-full" href="<?php echo $btnurl; ?>"><?php echo $btntxt; ?></a>
								 </div>

							</div>
						</li>
<?php $counter++ ; endwhile; endif;
$post = $old;
?>
				</ul>	
		<?php } ?>
		</div>
	</div>
</div>	