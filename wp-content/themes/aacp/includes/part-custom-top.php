<?php
$attach_topsection = get_post_meta($post->ID, 'iweb_top_customsections', TRUE);

	if( $attach_topsection && $attach_topsection !='' ){
		global $tmp_post;
		$tmp_post = $post;		
		$topsection_post = get_post($attach_topsection); 
		setup_postdata( $topsection_post );
		$post = $topsection_post;
		$bgsection_type = get_post_meta($post->ID, 'iweb_csection_bg', TRUE);
?>
<?php
	$bgclass = '';
	if( $bgsection_type =='Transparent' ){
		$bgclass = 'no-bg';
	} else 	if( $bgsection_type =='White' ){
		$bgclass = 'white-bg';
	} else 	if( $bgsection_type =='Light grey' ){
		$bgclass = 'light-grey-bg';
	} else 	if( $bgsection_type =='Dark grey' ){
		$bgclass = 'dark-grey-bg';
	}
?>
<div class="<?php echo $bgclass; ?>">
<?php the_content(); ?>
</div>
<?php
	wp_reset_postdata();
	$post = $tmp_post;
}?>