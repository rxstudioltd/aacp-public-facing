<?php
/**
 * @package WordPress
 */
?>
<?php
$post_author = get_post_meta($post->ID, 'iweb_post_author', TRUE); 
$feathumb = '';
?>
<div class="blog-item m-bot-35 clearfix">
						<div class="hover-item">
							<div class="clearfix">
								<div class="blog-item-date-inf-container left">
										<div class="blog-item-date-cont">
											<div class="blog-item-date"><?php the_time('d'); ?></div>
											<div class="blog-item-mounth"><?php the_time('M'); ?></div>
										</div>
										<div>
											<div class="blog-item-category-img">
												<img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/icon-video-post.png" alt="" />
											</div>
										</div>
								</div>
								<div class="view view-first">
							<?php 
							$video_embed = get_post_meta($post->ID, '_format_video_embed', true);
							if($video_embed != '')  { ?>
							<div class="video-container">
								<?php echo $video_embed; ?>
							</div>
							<?php } else { ?>
								<?php if(has_post_thumbnail())  { 
									$featimg = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
									$feathumb = $featimg[0];
								?>
								<?php } ?>
								<?php if ($feathumb != '') { ?>
									<img src="<?php echo $feathumb; ?>" alt="<?php the_title(); ?>">
								<?php } } ?>	
								
									<?php if ($feathumb != '') { ?>
									<div class="mask"></div>	
									<div class="abs">
										
										<a href="<?php echo $feathumb; ?>" class="lightbox zoom info"></a>
										
										<a href="<?php the_permalink(); ?>" class="link info"></a>
									</div>	
									<?php } ?>
								</div>
							</div>						
							<div class="blog-item-caption-container">
										<a class="a-invert" href="<?php the_permalink(); ?>" >
								<?php
									$title=get_the_title();
									$title=explode(' ',$title);
									$title[0]='<span class="bold">'.$title[0].'</span>';
									$title=implode(' ',$title);
								?>
								<?php echo $title; ?></a>
									<div class="lp-item-container-border clearfix">
										<div class="blog-info-container">
												<ul class="clearfix">
													<li class="author"><?php the_author(); ?></li>
													<li class="view"><?php echo getPostViews(get_the_ID()); ?></li>
													<li class="comment"><?php comments_number(__('0 Comment', 'iwebtheme'), __('1 Comment', 'iwebtheme'), __('% Comments', 'iwebtheme') );?></li>
													<li class="tag"><?php $the_tags = get_the_tags(); 
										if ($the_tags != '') {   foreach($the_tags as $tag) { echo $tag->name . ' . '; } } else { echo __('No tags', 'iwebtheme'); } ?></li>
												</ul>
										</div>
									</div>
							</div>
						</div>
						<div class="blog-item-text-container">
							<?php the_content(); ?> 
							<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
						</div>
</div>
<?php if($post_author != 'Disable') { ?>
	<!-- AUTHOR -->
	<div class="author-comment content-container-white m-bot-25">
		<div class="author-avatar">
		<?php echo get_avatar($comment,$size='92'); ?>
		</div>
		<div class="comment-head">
			<div class="author-name"><?php the_author_posts_link(); ?></div>
		</div>	
		<div class="author-text">
		<p><?php the_author_meta('description'); ?></p>
		</div>
	</div>
<?php } ?>