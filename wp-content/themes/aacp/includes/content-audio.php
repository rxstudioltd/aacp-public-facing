<?php
/**
 * @package WordPress
 */
?>
<?php 
$feathumb = '';
$featimg = '';
?>
<div class="blog-item m-bot-35 clearfix">
						<div class="hover-item">
							<div class="clearfix">
							<?php 
							$audio_embed = get_post_meta($post->ID, '_format_audio_embed', true);
							if(has_post_thumbnail())  { 
									$featimg = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
									$feathumb = $featimg[0];
							}
							if(($audio_embed == '') && ($featimg == ''))  { ?>
								<div class="blog-item-date-inf-container">								
							<?php } else { ?>
								<div class="blog-item-date-inf-container left">
								<?php } ?>
										<div class="blog-item-date-cont">
											<div class="blog-item-date"><?php the_time('d'); ?></div>
											<div class="blog-item-mounth"><?php the_time('M'); ?></div>
										</div>
										<div>
											<div class="blog-item-category-img">
												<img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/icon-audio-post.png" alt="" />
											</div>
										</div>
								</div>
								<div class="view view-first">
							<?php 
							if($audio_embed != '')  { ?>
							<div class="audio-container">
								<?php echo $audio_embed; ?>
							</div>
							<?php } else { ?>
									<?php if ($feathumb != '') { ?>
									<img src="<?php echo $feathumb; ?>" alt="<?php the_title(); ?>">
								<?php } } ?>	
								
									<?php if ($feathumb != '') { ?>
									<div class="mask"></div>	
									<div class="abs">
										
										<a href="<?php echo $feathumb; ?>" class="lightbox zoom info"></a>
										
										<a href="<?php the_permalink(); ?>" class="link info"></a>
									</div>	
									<?php } ?>
								</div>
							</div>
							<div class="blog-item-caption-container">
										<a class="a-invert" href="<?php the_permalink(); ?>" >
								<?php
									$title=get_the_title();
									$title=explode(' ',$title);
									$title[0]='<span class="bold">'.$title[0].'</span>';
									$title=implode(' ',$title);
								?>
								<?php echo $title; ?></a>
									<div class="lp-item-container-border clearfix">
										<div class="blog-info-container">
												<ul class="clearfix">
													<li class="author"><?php the_author(); ?></li>
													<li class="view"><?php echo getPostViews(get_the_ID()); ?></li>
													<li class="comment"><?php comments_number(__('0 Comment', 'iwebtheme'), __('1 Comment', 'iwebtheme'), __('% Comments', 'iwebtheme') );?></li>
													<li class="tag"><?php $the_tags = get_the_tags(); 
										if ($the_tags != '') {   foreach($the_tags as $tag) { echo $tag->name . ' . '; } } else { echo __('No tags', 'iwebtheme'); } ?></li>
												</ul>
										</div>
									</div>
							</div>					
				
							
						</div>
						<div class="blog-item-text-container">
							<?php
							$trimcontent = get_the_content();
							$shortexcerpt = wp_trim_words($trimcontent,$num_words = 80);
							?>
							<p><?php echo $shortexcerpt; ?></p>
						</div>
						<div class="lp-r-m-container right">
						<a href="<?php the_permalink(); ?>" class="button medium r-m-plus r-m-full"><?php echo __('READ MORE', 'iwebtheme'); ?></a>
						</div>
</div>