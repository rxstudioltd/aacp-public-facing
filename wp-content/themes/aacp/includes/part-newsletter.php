<?php
/**
 * @package WordPress
 */
// vars
$mc_title = iwebtheme_smof_data('mc_title');
$mc_text = iwebtheme_smof_data('mc_text');
?>
<!-- NEWS LETTER -->
<div class="dark-grey-bg">
	<div class="container m-bot-20 clearfix">
		<div class="sixteen columns">
			<div class="newsletter-container clearfix">
				<div class="nl-img-container">
					<img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/link-icon@2x.png" alt="" />
				</div>
				<div class="nl-text-container clearfix">
					<div class="caption">
						<?php echo $mc_title; ?>
					</div>
					<div class="nl-text"><?php echo $mc_text; ?></div>
					<div class="nl-form-container">
						<a href="http://www.aacp.org.uk/" target="_blank"><div class="nl-button">Click here</div></a>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>