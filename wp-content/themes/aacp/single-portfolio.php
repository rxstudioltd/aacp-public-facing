<?php
/**
 * @package WordPress
 */
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
?>
<?php
get_header(); 
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<?php get_template_part('includes/breadcrumbs'); ?>
				<ul class="portfolio-pagination">
				<?php
				if( function_exists('be_previous_post_link') ):
				$postlist_args = array(
				   'posts_per_page'  => -1,
				   'orderby'         => 'menu_order title',
				   'order'           => 'ASC',
				   'post_type'       => 'portfolio',
				   'taxonomy' => 'portfolio_categories'
				); 
				$postlist = get_posts( $postlist_args );

				// get ids of posts retrieved from get_posts
				$ids = array();
				foreach ($postlist as $thepost) {
				   $ids[] = $thepost->ID;
				}
				// get and echo previous and next post in the same taxonomy        
				$thisindex = array_search($post->ID, $ids);
				$previd = $ids[$thisindex-1];
				$nextid = $ids[$thisindex+1];
				if ( !empty($previd) ) {
				   echo '<li><a class="pag-prev" rel="prev" href="' . get_permalink($previd). '"></a></li>';
				}
				if ( !empty($nextid) ) {
				   echo '<li><a class="pag-next" rel="next" href="' . get_permalink($nextid). '"></a></li>';
				}	
				endif;
				?>
				</ul>				
			</div>	
			
		</div>
	</div>	
</div>	<!-- Grey bg end -->
<div class="container clearfix">

	<?php if ($sidebar_pos == 'left') { ?>
		<?php get_sidebar('portfolio'); ?>
	<?php } ?>	
	<div class="eleven columns m-bot-25">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								<?php
									$title=get_the_title();
									$title=explode(' ',$title);
									$title[0]='<span class="bold">'.$title[0].'</span>';
									$title=implode(' ',$title);
					if (has_post_thumbnail()) {					
						$image_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
						$image_url = $image_src [0];					
					}
					?>

	
			<div class="blog-item m-bot-35 clearfix">
						<div class="hover-item">
							<div class="clearfix">
								<div class="view view-first">
									<img src="<?php echo $image_url; ?>" alt="" />		
									<div class="mask"></div>								
									<div class="abs">									
											<a class="lightbox zoom info" href="<?php echo $image_url; ?>"></a>
									</div>
								</div>
							</div>
							<div class="blog-item-caption-container">
								<a class="a-invert" href="#" ><?php echo $title; ?></a>

							</div>							
						</div>
						<div class="blog-item-text-container">
							<?php the_content(); ?> 
						</div>

			</div>
			
	<?php endwhile; ?>
	<?php endif; ?>
	</div>	
	<?php if ($sidebar_pos == 'right') { ?>
		<?php get_sidebar('portfolio'); ?>
	<?php } ?>

</div>    
<?php if($mb_portfolio != 'Disable') { ?>
	<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php } ?>
<?php if($mb_signup != 'Disable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>
<?php if($mb_clients != 'Disable') { ?>
	<?php get_template_part( 'includes/part-clients' ); ?>
<?php } ?>	
<?php get_footer(); ?>