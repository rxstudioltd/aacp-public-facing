<?php
/**
 * @package WordPress
 */
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
?>
<?php get_header(); ?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
			<?php if ($page_title != '') { ?>
				<h1 class="page-title"><?php echo $page_title; ?></h1>
			<?php } else { ?>
				<?php get_template_part('includes/breadcrumbs'); ?>	
			<?php } ?>
			</div>	
		</div>
	</div>	
</div>	<!-- Grey bg end -->
<div class="container m-bot-35 clearfix">
	<?php if ($sidebar_pos == 'left') { ?>
		<?php get_sidebar(); ?>
	<?php } ?>
		<div class="eleven columns">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>	
			<h3><?php the_title();?></h3>	
			<?php the_content(); ?>
			<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
			<?php endwhile; endif; ?>

			<?php comments_template(); ?>				
		</div>
	<?php if ($sidebar_pos == 'right') { ?>
		<?php get_sidebar(); ?>
	<?php } ?>	

</div>             
<?php get_footer(); ?>