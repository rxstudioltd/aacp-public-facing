<?php
/**
 * @package WordPress
 */
if (empty($feed_url)) { $feed_url = get_bloginfo('rss2_url'); }
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- Favicons
	================================================== -->
<?php
	$favicon = ''; $favicon = iwebtheme_smof_data('favicon');
	if (empty($favicon)) { ?>
	<link link rel="shortcut icon" href="<?php echo get_template_directory_uri().'/images/favicon.png' ?>" />
<?php }	else { ?>
	<link rel="icon" type="image/png" href="<?php echo $favicon ?>" />
<?php } ?>	
<!-- wp head -->
<?php 
wp_head(); 
?>

</head>
<body <?php body_class( 'body' ); ?>>

<div id="wrap" class="boxed">
<div class="grey-bg"> <!-- Grey bg  -->	

	<!--[if lte IE 7]>
	<div id="ie-container">
		<div id="ie-cont-close">
			<a href='#' onclick='javascript&#058;this.parentNode.parentNode.style.display="none"; return false;'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-close.jpg' style='border: none;' alt='Close'></a>
		</div>
		<div id="ie-cont-content" >
			<div id="ie-cont-warning">
				<img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning.jpg' alt='Warning!'>
			</div>
			<div id="ie-cont-text" >
				<div id="ie-text-bold">
					You are using an outdated browser
				</div>
				<div id="ie-text">
					For a better experience using this site, please upgrade to a modern web browser.
				</div>
			</div>
			<div id="ie-cont-brows" >
				<a href='http://www.firefox.com' target='_blank'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-firefox.jpg' alt='Download Firefox'></a>
				<a href='http://www.opera.com/download/' target='_blank'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-opera.jpg' alt='Download Opera'></a>
				<a href='http://www.apple.com/safari/download/' target='_blank'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-safari.jpg' alt='Download Safari'></a>
				<a href='http://www.google.com/chrome' target='_blank'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-chrome.jpg' alt='Download Google Chrome'></a>
			</div>
		</div>
	</div>
	<![endif]-->
		<header id="header">
			<div class="container clearfix">
				<div class="sixteen columns header-position">

					<div class="header-container m-top-30 clearfix">
						
						<div class="header-logo-container ">
							<div class="logo-container">	
		
							<?php if(iwebtheme_smof_data('top_logoretina') !='') { ?>
									<a href="<?php echo home_url(); ?>/" title="<?php bloginfo( 'name' ); ?>" class="logo" rel="home">
									<img src="<?php echo iwebtheme_smof_data('top_logoretina'); ?>" width="<?php echo iwebtheme_smof_data('toplogo_w'); ?>" height="<?php echo iwebtheme_smof_data('toplogo_h'); ?>" class="logo_retina" alt="<?php bloginfo( 'name' ) ?>" />
									</a>
							<?php } ?>						
							<?php if(iwebtheme_smof_data('top_logoretina') =='') { ?>					
								<a href="<?php echo home_url(); ?>/" title="<?php bloginfo( 'name' ); ?>" class="logo">
								<img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/logo.png" alt="" /></a>
							<?php } ?>
							</div>
						</div>

						<div class="header-menu-container right">	
								<?php 
									/* Small Header Menu */
									dynamic_sidebar('Small Header Menu');
								?>
								<nav id="main-nav">
									<!-- TOP MENU -->
									<?php
									wp_nav_menu(array(
									'menu_class' => 'sf-menu clearfix',
									'theme_location' => 'main',
									'fallback_cb' => 'pages',
									'depth' => 0
									));
									?>	
								</nav>	
								<?php if(iwebtheme_smof_data('sw_search') != 0) { ?>
								<div class="search-container ">
									<form action="<?php echo home_url(); ?>" class="search-form">
										<input type="text" name="s" id="s" class="search-text" >
										<input type="submit" value="" class="search-submit" name="submit">
									</form>
								</div>
								<?php } ?>
						</div>

					
					</div>
				</div>
			</div>

		</header>