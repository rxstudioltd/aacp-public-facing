<?php
/**
 * @package WordPress
 */
?>

<!-- FOOTER -->
	<footer>
		<div class="footer-content-bg">
			<?php if(iwebtheme_smof_data('sw_bottomcontent') != 0)  { ?>
			<div class="container clearfix">
				<div class="one-third-footer-spec column omega">
				
				
						<div class="logo-footer-container">	
				
						<?php if(iwebtheme_smof_data('footer_logoretina') !='') { ?>
								<a href="<?php echo home_url(); ?>/" title="<?php bloginfo( 'name' ); ?>" class="logo" rel="home">
								<img src="<?php echo iwebtheme_smof_data('footer_logoretina'); ?>" width="<?php echo iwebtheme_smof_data('footerlogo_w'); ?>" height="<?php echo iwebtheme_smof_data('footerlogo_h'); ?>" class="footerlogo_retina" alt="<?php bloginfo( 'name' ) ?>" />
								</a>
						<?php } ?>						
						<?php if(iwebtheme_smof_data('footer_logoretina') =='') { ?>					
							<a href="<?php echo home_url(); ?>/" title="<?php bloginfo( 'name' ); ?>" class="logo">
							<img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/logo.png" alt=""/>
							</a>
						<?php } ?>
						</div>
				
				</div>
				<div class="two-thirds-footer-spec column alpha">
					<p class="footer-content-container m-none"><?php echo iwebtheme_smof_data('bottom_text'); ?></p>
				</div>
			</div>
			<?php } ?>
			
			<div class="container clearfix">
				<div class="one-third column">
					<?php dynamic_sidebar('footer-1'); ?>
				</div>
				<div class="one-third column ">
					<?php dynamic_sidebar('footer-2'); ?>
				</div>
				<div class="one-third column ">
					<?php dynamic_sidebar('footer-3'); ?>
				</div>
			</div>			
			
			
		</div>
		<div class="footer-copyright-bg">
			<div class="container ">
				<div class="sixteen columns clearfix">
					<div class="footer-copyright-container">
						<?php echo iwebtheme_smof_data('footer_text'); ?>
					</div>
				</div>
				
			</div>
		</div>
	</footer>	
		<p id="back-top">
			<a href="#top" title="<?php echo __('Back to Top','iwebtheme'); ?>"><span></span></a>
		</p>
</div><!-- End wrap -->
	
<?php echo stripslashes(iwebtheme_smof_data('google_analytics')); ?>
<?php wp_footer(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42495199-15', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>