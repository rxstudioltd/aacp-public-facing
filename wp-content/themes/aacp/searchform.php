<?php
/**
 * @package WordPress
 * @subpackage Solana Theme
 */
?>
<form action="<?php echo home_url(); ?>" class="search-form">
	<input type="text" name="s" id="s" placeholder="Start Searching ..." class="search-text" />
    <input type="submit" value="" class="search-submit" name="submit">
    <!-- end search_form -->
</form>
<div class="clearfix"></div>