<?php
/**
 * @package WordPress
 */
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
?>
<?php get_header(); ?>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<?php /* If this is a category archive */ if (is_category()) { ?>
                <h1 class="page-title"><?php echo __('Archive for', 'iwebtheme'); ?> &#8216;<?php single_cat_title(); ?>&#8217; <?php echo __('Category', 'iwebtheme'); ?></h1>
				<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h1 class="page-title"><?php echo __('Posts Tagged', 'iwebtheme'); ?> &#8216;<?php single_tag_title(); ?>&#8217;</h1>
				<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h1 class="page-title"><?php echo __('Archive for', 'iwebtheme'); ?> <?php the_time('F jS, Y'); ?></h1>
				<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h1 class="page-title"><?php echo __('Archive for', 'iwebtheme'); ?> <?php the_time('F, Y'); ?></h1>
				<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h1 class="page-title"><?php echo __('Archive for', 'iwebtheme'); ?> <?php the_time('Y'); ?></h1>
				<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h1 class="page-title"><?php echo __('Author Archive', 'iwebtheme'); ?></h1>
				<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h1 class="page-title"><?php echo __('Blog Archives', 'iwebtheme'); ?></h1>
				<?php } ?>
			</div>	
		</div>
	</div>	
</div>	<!-- Grey bg end -->
	<div class="container clearfix">

	<?php if ($sidebar_pos == 'left') { ?>
		<?php get_sidebar(); ?>
	<?php } ?>	
	<?php $taxonomy_exist = taxonomy_exists('portfolio_categories'); ?>
	<?php if ( $taxonomy_exist = true ) { ?>
	<div class="eleven columns m-bot-25">

		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'includes/content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php if (function_exists("pagination")) { ?>
			<div class="pagination-1-container">
			<?php pagination(); ?>
			</div>
			<?php } else {
			posts_nav_link(' &#183; ', 'previous page', 'next page'); 	
			} ?>
		<?php endif; // end have_posts() check ?>

	</div>
	<?php } ?>
	<?php if ($sidebar_pos == 'right') { ?>
		<?php get_sidebar(); ?>
	<?php } ?>
	
	</div>
<!--end content-->
<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php get_template_part( 'includes/part-clients' ); ?>
<?php get_footer(); ?>