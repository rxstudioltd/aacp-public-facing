<?php
get_header(); 
$page_title = iwebtheme_smof_data('error_title');
?>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<h1 class="page-title"><?php echo $page_title; ?></h1>
			</div>	
		</div>
	</div>	
</div>	<!-- Grey bg end -->
<!-- 404 -->
	<div class="container m-bot-100 m-top-80 clearfix">
		<div class="eight columns ">
		
			<div class="error404-numb">
				404
			</div>
			
		</div>
		<div class="eight columns ">		
			<div class="error404-text">
				<?php echo iwebtheme_smof_data('error_content'); ?>
			</div>
			
		</div>
	</div>
<!-- 404 2 -->
	
<div class="light-grey-bg m-top-30">
	<div class="container clearfix">
		
		<div class="sixteen columns content-container-white ">
			
				<div class="error404-main-text">
					<h2><?php echo iwebtheme_smof_data('error_morecontent'); ?></h2>
				</div>
				
		</div>
		
	</div>
</div>

<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php get_template_part( 'includes/part-clients' ); ?>
<?php get_footer(); ?>