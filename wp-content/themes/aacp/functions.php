<?php
/**
 * @package WordPress
 * @subpackage Solana Theme
*/

// Set the content width
if ( ! isset( $content_width )) 
    $content_width = 960;
	
/*============================================
Includes 
=============================================*/
require_once('functions/aq_resizer.php');
require_once('functions/theme-functions.php');
require_once('functions/custom-post.php');
require_once('functions/comments.php');
require_once('functions/googlefont.php');
// Admin 
require_once ('admin/index.php');

/* Global admin options */
if ( ! function_exists('iwebtheme_smof_data') ) {
	function iwebtheme_smof_data($id, $fallback = false) {
		global $smof_data;
		if ( $fallback == false ) $fallback = '';
		$output = ( isset($smof_data[$id]) && $smof_data[$id] !== '' ) ? $smof_data[$id] : $fallback;
		return $output;
	}
}
global $post;

function iwebtheme_solana_theme_setup() {

/* Theme supports */
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-formats', array('gallery', 'link', 'quote', 'audio', 'video')); 
/* --- post image functionality --- */
if ( function_exists( 'add_theme_support' ))
	add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'full-size',  9999, 9999, false );
	add_image_size( 'small-thumb',  54, 54, true );
}
/* --- Localization --- */
load_theme_textdomain( 'iwebtheme', get_template_directory().'/lang' );

/* --- action --- */
add_action('wp_enqueue_scripts', 'iwebtheme_enqueue_css');
add_action('wp_head','iwebtheme_build_custom_css'); 
add_action('wp_enqueue_scripts', 'iwebtheme_enqueue_js');
add_action( 'tgmpa_register', 'iwebtheme_theme_register_required_plugins' );
add_action('wp_footer','iwebtheme_custom_js',100);

/* --- Filters --- */
add_filter('widget_text', 'do_shortcode');
add_filter( 'body_class', 'iwebtheme_body_class');
add_filter( 'body_class', 'iwebtheme_body_boxed');
}
add_action( 'after_setup_theme', 'iwebtheme_solana_theme_setup' );

/*============================================
 css
=============================================*/
function iwebtheme_enqueue_css() {	
	global $is_IE;

	//load css files
	wp_enqueue_style('style', get_template_directory_uri(). '/style.css', 'style');
	wp_enqueue_style('skeleton', get_template_directory_uri() . '/css/skeleton.css', 'style');
	wp_enqueue_style('fancybox', get_template_directory_uri() . '/css/jquery.fancybox-1.3.4.css', 'style');
	
	if( iwebtheme_smof_data('layout_mode') =='wide' ) {
		wp_enqueue_style('wide', get_template_directory_uri() . '/css/layout/wide.css', 'style');
	} else {
		wp_enqueue_style('boxed', get_template_directory_uri() . '/css/layout/boxed.css', 'style');
	}

	
    // Register and enqueue lt IE8 stylesheet
    wp_register_style( 'ie-warning', get_template_directory_uri() . '/css/ie-warning.css' );
    $GLOBALS['wp_styles']->add_data( 'ie-warning', 'conditional', 'lt IE 8' );
    wp_enqueue_style( 'ie-warning' );
    // Register and enqueue lt IE9 stylesheet
    wp_register_style( 'style-ie', get_template_directory_uri() . '/css/style-ie.css' );
    $GLOBALS['wp_styles']->add_data( 'style-ie', 'conditional', 'lt IE 9' );
    wp_enqueue_style( 'style-ie' );
    // Register and enqueue lt IE8 stylesheet
    wp_register_style( 'ei8fix', get_template_directory_uri() . '/css/ei8fix.css' );
    $GLOBALS['wp_styles']->add_data( 'ei8fix', 'conditional', 'lt IE 8' );
    wp_enqueue_style( 'ei8fix' );

	if(!is_page_template('templates/page-home-1.php') && !is_page_template('templates/page-home-2.php')) {
	wp_enqueue_style('flexslider-post', get_template_directory_uri() . '/css/flexslider-post.css', 'style');
	}
	if(is_page_template('templates/page-home-1.php')) {
	wp_enqueue_style('flexslider', get_template_directory_uri() . '/css/flexslider.css', 'style');
	}
	if(is_page_template('templates/page-home-2.php')) {
	wp_enqueue_style('flexslider', get_template_directory_uri() . '/css/flexslider2.css', 'style');
	}	
	if(is_page_template('templates/page-home-3.php')) {
	wp_enqueue_style('sequencejs', get_template_directory_uri() . '/css/sequencejs-theme.modern-slide-in.css', 'style');
	}
	if(is_page_template('templates/page-home-4.php')) {
	wp_enqueue_style('sequencejs', get_template_directory_uri() . '/css/sequencejs-theme.modern-slide-in2.css', 'style');
	}	

	if( iwebtheme_smof_data('alt_stylesheet')!='' ) {
		$premadeskin = iwebtheme_smof_data('alt_stylesheet');
		wp_enqueue_style('skin', get_template_directory_uri() . '/css/colors/'.$premadeskin, 'skin');
	}	

	if(( iwebtheme_smof_data('alt_bg') != '') && (iwebtheme_smof_data('switch_bgupload') != 1)) {
		$premadebg = iwebtheme_smof_data('alt_bg');
			wp_enqueue_style('background', get_template_directory_uri() . '/css/bg/'.$premadebg, 'background');
	}	


}


/*============================================
body class
=============================================*/
function iwebtheme_body_class( $classes ) {
     if ( is_page_template('templates/page-home-1.php') || is_page_template('templates/page-home-2.php') || is_page_template('templates/page-home-3.php') || is_page_template('templates/page-home-4.php') || is_page_template('templates/page-home-5.php') ) {
          $classes[] = 'homepage-head';
		}
     return $classes;
}

function iwebtheme_body_boxed( $classes ) {
     if( iwebtheme_smof_data('layout_mode') =='boxed' ) {
          $classes[] = 'bg-1';
	}
     return $classes;
}


/*============================================
Custom css
=============================================*/
function iwebtheme_build_custom_css(){
		$iwebtheme_buildcss = '';
		$iwebtheme_buildcss .= iwebtheme_setting_css();
		$iwebtheme_buildcss .= iwebtheme_option_css();

		if(!empty($iwebtheme_buildcss)){
			$iwebtheme_wrap_buildcss ='';
			$iwebtheme_wrap_buildcss .="<style type=\"text/css\">\n";
			$iwebtheme_wrap_buildcss .= $iwebtheme_buildcss;
			$iwebtheme_wrap_buildcss .="</style>\n";
			echo $iwebtheme_wrap_buildcss;
		}
}

// get custom css from theme options
function iwebtheme_option_css(){
	$iwebtheme_custom_css = iwebtheme_smof_data( 'custom_css' );
	
	
		if(iwebtheme_smof_data( 'custom_css' ) != '') {
			$iwebtheme_custom_css = iwebtheme_smof_data( 'custom_css' );
			return "\n".$iwebtheme_custom_css."\n";
		}
}

function iwebtheme_setting_css(){

	//custom color
	$setting_css = '';
	$custom_color = iwebtheme_smof_data('own_color');
		if ($custom_color != '') {		
			$setting_css .= ".search-submit:hover,.facebook-link, .skype-link, .twitter-link, .flickr-link, .vimeo-link, .dribbble-link, .linkedin-link, .rss-link,.googleplus-link, 
			.pintrest-link,#main-nav .current > a,#main-nav .current li.current a, #main-nav .current li.current li.current a,#main-nav .current-menu-ancestor a, #main-nav .current_page_parent a, 
			#main-nav .current-menu-item a,#main-nav a:hover,.block-1-content-container,.block-black-text,.box2-img-container,.block2-a:hover .box2-img-container-inv,.box3-img-container,
			.block3-a:hover .box3-img-container-inv,.box5-img-container, .box5-img-container-inv,.block5-a:hover .box5-img-container-inv,.buy-img-container,.button-buy-container a,.nl-img-container,.nl-button,
			.jcarousel-next:hover, .jcarousel-prev:hover,.bg-45,.view-first .mask,.page-title-container,.contact-loc:after,.contact-phone:after,.contact-mail:after,.contact-mail a:hover,.blog-item-date-cont, .blog-item-title-cont a:hover,.pagination-1 a:hover,.pagination-1 a.pag-current,
			.pagination-1 .pag-prev:hover,.pagination-1 .pag-next:hover,.blog-categories li a:hover .blog-cat-icon,.blog-categories li.active .blog-cat-icon,.ui-accordion-header:hover span,.ui-accordion-header-active span,.tagcloud ul.wp-tag-cloud li a:hover,.comment-reply a,.box-icon:hover i,
			.price-col-main h1,.pt-col-main,h1.pt-col-main,a.r-m-full:hover,.button,.button.gray:hover,.button.get-theme,.view a.zoom:hover, .view a.link:hover,#back-top a:hover,.flex-control-paging li a:hover,.flex-control-paging li a:hover,
.flex-control-paging li a.flex-active:hover,.sequence-theme-6 #sequence-theme .next:hover,.sequence-theme-6 #sequence-theme .prev:hover,.flexslider .flex-next:hover,.flexslider .flex-prev:hover,.blog-categories li.active .blog-cat-icon, .blog-categories li:first-child .blog-cat-icon,blockquote:before,.tagcloud a:hover{";
			$setting_css .= "background-color:".$custom_color.";";
			$setting_css .= "}";
			$setting_css .= ".flex-direction-nav li a:hover,.slider-1 .slides li h2,.flex-direction-nav li a:hover,.r-m-plus:after,a.r-m-plus-small{";
			$setting_css .= "background-color:".$custom_color." !important;";
			$setting_css .= "}";
			$setting_css .= ".dropcap4 {";
			$setting_css .= "background: none repeat scroll 0 0 ".$custom_color." !important;";
			$setting_css .= "}";
			$setting_css .= "#sequence-theme .nav li img.active {";
			$setting_css .= "border-top: 5px solid ".$custom_color." !important;";
			$setting_css .= "}";
			$setting_css .= "#sequence-theme .main-text {";
			$setting_css .= "border-left: 5px solid ".$custom_color." !important;";
			$setting_css .= "}";
			$setting_css .= ".box3-description-container {";
			$setting_css .= "border-bottom: 3px solid ".$custom_color." !important;";
			$setting_css .= "}";
			$setting_css .= ".latest-post-sidebar img:hover {";
			$setting_css .= "border-color: ".$custom_color." !important;";
			$setting_css .= "background: none repeat scroll 0 0 ".$custom_color.";";
			$setting_css .= "}";
			$setting_css .= "::selection {";
			$setting_css .= "background-color:".$custom_color." !important;";
			$setting_css .= "}";
			$setting_css .= "::-moz-selection {";
			$setting_css .= "background-color:".$custom_color." !important;";
			$setting_css .= "}";
			$setting_css .= ".sf-menu li li a:hover,.pagination-1 a.pag-current,.skill-bar-content {";
			$setting_css .= "background:".$custom_color." !important;";
			$setting_css .= "}";
			$setting_css .= ".contact-mail a:hover,.blog-categories li a:hover,.ui-accordion-header:hover a,.ui-accordion-header-active a,.comment-name a:hover,.author-name a:hover,#toggle-view .ui-accordion-header-active,#toggle-view .ui-accordion-header:hover,.latest-post h4 a:hover,.footer-mail a:hover,#footer-nav li a:hover,a.author:hover,.tweet_text a:hover, .tweet_time a:hover,a,.a-invert:hover {";
			$setting_css .= "color:".$custom_color.";";
			$setting_css .= "}";
			$setting_css .= "#sequence-theme h3,#sequence .slide-2-yellow {";
			$setting_css .= "color:".$custom_color." !important;";
			$setting_css .= "}";
		}
		
		// bg color
		$body_background = '';
		$body_background = iwebtheme_smof_data('body_background');
		
		if ($body_background != '') {		
			$setting_css .= "body {";
			$setting_css .= "background-image: none;";
			$setting_css .= "background:".$body_background." !important;";
			$setting_css .= "}";
		}
		// bg custom upload
		$bg_bodyupload = '';
		$bg_uploadprop = '';
		
		$bg_bodyupload = iwebtheme_smof_data('bg_bodyupload');
		$bg_uploadprop = iwebtheme_smof_data('bg_uploadprop');
		
		if ((iwebtheme_smof_data('switch_bgupload') != 0) && ($bg_bodyupload != '')) {		
			$setting_css .= "body {";
			$setting_css .= "background-image: url(".$bg_bodyupload.");";	
			$setting_css .= "background-repeat:".$bg_uploadprop.";";			
			$setting_css .= "}";
		}
		
		$retina_toplogo = iwebtheme_smof_data('top_logoretina');	
		$toplogo_w = iwebtheme_smof_data('toplogo_w');	
		$toplogo_h = iwebtheme_smof_data('toplogo_h');	
		
		if (($retina_toplogo != '')) {			
			// create css
			$setting_css .= "img.logo_retina {";
			if ($retina_toplogo != '') {	
			$setting_css .= "display: block;";
			$setting_css .= "width: ".$toplogo_w."px;";
			$setting_css .= "height: ".$toplogo_h."px;";
			}
			$setting_css .= "}";
		}
		$retina_footerlogo = iwebtheme_smof_data('footer_logoretina');	
		$footerlogo_w = iwebtheme_smof_data('footerlogo_w');	
		$footerlogo_h = iwebtheme_smof_data('footerlogo_h');	
		
		if (($retina_footerlogo != '')) {			
			// create css
			$setting_css .= "img.footerlogo_retina {";
			if ($retina_footerlogo != '') {	
			$setting_css .= "display: block;";
			$setting_css .= "width: ".$footerlogo_w."px;";
			$setting_css .= "height: ".$footerlogo_h."px;";
			}
			$setting_css .= "}";
		}
		
		$footerlogo_pad = iwebtheme_smof_data('footerlogo_pad');
		if (($footerlogo_pad != '')) {			
			// create css
			$setting_css .= ".logo-footer-container a {";
			if ($footerlogo_pad != '') {	
			$setting_css .= "padding: ".$footerlogo_pad.";";
			}
			$setting_css .= "}";
		}	

		// typography
		$check_defaultfont = '';
		$check_defaultfont = iwebtheme_smof_data('check_defaultfont');
		$body_standardfont = '';
		$body_standardfont = iwebtheme_smof_data('body_standardfont');
		$check_gfont = '';
		$check_gfont = iwebtheme_smof_data('check_gfont');
		$body_gfont = '';
		$body_gfont = iwebtheme_smof_data('_font');
		$check_fontprop = '';
		$check_fontprop = iwebtheme_smof_data('check_fontprop');
		$body_fontstyle = iwebtheme_smof_data('body_fontstyle');
		$check_hdefaultfont = '';
		$check_hdefaultfont = iwebtheme_smof_data('check_hdefaultfont');
		$heading_fontstyle = iwebtheme_smof_data('heading_fontstyle');
		$check_gfontheading = '';
		$check_gfontheading = iwebtheme_smof_data('check_gfontheading');
		$_headingfont = '';
		$_headingfont = iwebtheme_smof_data('_headingfont');
		
		
		//body
		if (($check_defaultfont != 0) && ($body_standardfont['face'] != 'default') ) {	
			$setting_css .= "body {";
			$setting_css .= "font-family:".$body_standardfont['face'].";";
			$setting_css .= "}";
		}
		if (($check_gfont != 0) && ($body_gfont != 'Open Sans') ) {	
		echo "<link href='http://fonts.googleapis.com/css?family=$body_gfont' rel='stylesheet' type='text/css'>";
			$setting_css .= "body {";
			$setting_css .= "font-family:".$body_gfont.";";
			$setting_css .= "}";
		}	
		if ($check_fontprop != 0) {	
			$setting_css .= "body {";
			if ($body_fontstyle['size'] != '12px') {	
			$setting_css .= "font-size:".$body_fontstyle['size'].";";
			}
			if ($body_fontstyle['color'] != '#7e8082') {	
			$setting_css .= "color:".$body_fontstyle['color'].";";
			}
			if ($body_fontstyle['height'] != '18px') {	
			$setting_css .= "line-height:".$body_fontstyle['height'].";";
			}
			$setting_css .= "}";
		}		
		
		//heading & title
		if (($check_hdefaultfont != 0) && ($heading_fontstyle['face'] != 'OswaldLight') ) {	
			$setting_css .= "h1,h2,h3,h4,h5,h6,.search-title,.search-text,.sf-menu li > a,.ca-main,.box1-text-container h3,.box2-text-container h3,.box3-text-container h3,.box5-text-container h3,.buy-text h2,.button-buy-container a,.nl-button,.caption,input, textarea,.caption-text-container,.lw-item-caption-container a,.lp-caption-container,.lp-date-container,.lp-item-caption-container,.page-title,.error404-text,.error404-numb,.error404-main-text h2,.contant-container-caption,.title-block, .title-widget,.blog-item-date-cont,.blog-item-title,.all-comments-container,.comment-name,.comment-name a,.author-name a,.box-icon h3,#footer-nav li a,.price-col-gray h1,.month,.price-col-main h1,.currency, .price, .cents,.place2,.price-col-gray2 h1,.team-name h5,.button,nav#main-nav select  {";
			$setting_css .= "font-family:".$heading_fontstyle['face'].";";
			$setting_css .= "}";
		}		
		if (($check_gfontheading != 0) && ($_headingfont != '') ) {	
		echo "<link href='http://fonts.googleapis.com/css?family=$_headingfont' rel='stylesheet' type='text/css'>";
			$setting_css .= "h1,h2,h3,h4,h5,h6,.search-title,.search-text,.sf-menu li > a,.title-block-text,.block-text,.ca-main,.box2-text-container h3,.box3-text-container h3,.box5-text-container h3,.buy-text h2,.button-buy-container a,.nl-button,.caption,input, textarea,.lw-item-caption-container a,.lp-item-caption-container a,.caption-text-container,.caption-text-container-test-block,.page-title,.error404-text,.error404-numb,.error404-main-text h2,.contant-container-caption,.title-block, .title-widget,.blog-item-date-cont,.blog-item-title,.blog-item-caption-container a,.skill-bar .skill-title,.all-comments-container,.comment-name a,.author-name a,.box-icon h3,.features-2-text,.title-font-24,.title-font-12,.footer-content-container,#footer-nav li a,.price-col-gray h1,.month,.price-col-main h1,.currency, .price, .cents,.cents-cont,.place2,.price-col-gray2 h1,.team-name h5,.button,nav#main-nav select,.slider-1 .slides li h2,.slider-1 .slides li p,.flex-caption .title h2,#sequence-theme .title h2,#sequence-theme h2,#sequence-theme h3,#sequence .slide-2-yellow  {";
			$setting_css .= "font-family:".$_headingfont.";";
			$setting_css .= "}";
		}		
		
	return $setting_css;
}

/*============================================
Enqueue js
=============================================*/
function iwebtheme_enqueue_js() {
	
	//load js files
	wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-1.8.3.min.js', 'jquery');
	wp_enqueue_script('easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js','jquery', '1.3.0', TRUE);
	wp_enqueue_script('superfish', get_template_directory_uri() . '/js/superfish.js','jquery', '1.4.8', TRUE);
	wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js','jquery', '1.9.2', TRUE);
	wp_enqueue_script('tools', get_template_directory_uri() . '/js/tools.js','jquery', '1.1.1', TRUE);
	
	wp_enqueue_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js','jquery', '2.2.0', TRUE);

	if(is_page_template('templates/page-home-1.php')) {
		wp_enqueue_script('flex-slider', get_template_directory_uri() . '/js/flex-slider.js','jquery', '2.2.0', TRUE);
	}	
	if(is_page_template('templates/page-home-2.php')) {
		wp_enqueue_script('flex-slider2', get_template_directory_uri() . '/js/flex-slider2.js','jquery', '2.2.0', TRUE);
	}
	if(is_page_template('templates/page-home-3.php')) {
		wp_enqueue_script('sequence', get_template_directory_uri() . '/js/sequence.jquery-min.js','jquery', '0.8.2', TRUE);
		wp_enqueue_script('sequence-slider', get_template_directory_uri() . '/js/sequence-slider.js','jquery', '0.8.2', TRUE);
	}	
	if(is_page_template('templates/page-home-4.php')) {
		wp_enqueue_script('sequence', get_template_directory_uri() . '/js/sequence.jquery-min.js','jquery', '0.8.2', TRUE);
		wp_enqueue_script('sequence-slider', get_template_directory_uri() . '/js/sequence-slider2.js','jquery', '0.8.2', TRUE);
	}	
	wp_enqueue_script('jcarousel', get_template_directory_uri() . '/js/jquery.jcarousel.js','jquery', '0.3.0', TRUE);
	wp_enqueue_script('fancybox', get_template_directory_uri() . '/js/jquery.fancybox-1.3.4.pack.js','jquery', '1.3.4', TRUE);
	wp_enqueue_script('BlackAndWhite', get_template_directory_uri() . '/js/jQuery.BlackAndWhite.min.js','jquery', '0.2.4', TRUE);
	wp_enqueue_script('validate', get_template_directory_uri() . '/js/jquery.validate.min.js','jquery', '1.9.0', TRUE);
	wp_enqueue_script('jflickrfeed', get_template_directory_uri() . '/js/jflickrfeed.min.js','jquery', '1.3.1', TRUE);
	wp_enqueue_script('quicksand', get_template_directory_uri() . '/js/jquery.quicksand.js','jquery', '1.2.2', TRUE);

	if(is_page_template('templates/page-contact.php')) {
		wp_enqueue_script('maps','http://maps.google.com/maps/api/js?sensor=true','jquery', '1.3', TRUE);
		wp_enqueue_script('gmap', get_template_directory_uri() . '/js/jquery.gmap.min.js','jquery', '2.1.2', TRUE);
	}
	//load comment reply js
	if(is_single() || is_page()) {
		wp_enqueue_script('comment-reply');
	}
	wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js','jquery', '1.0.0', TRUE);

}

/*============================================
Custom js
=============================================*/

function iwebtheme_custom_js(){

	$script_out = '';
	$script_out .= '<script type="text/javascript">'."\n";
	$script_out .= 'jQuery(document).ready(function($){'."\n";
	$script_out .= iwebtheme_back_top();
	$script_out .= iwebtheme_mailchimp();
	if(is_page_template('templates/page-contact.php')) {
	$script_out .= iwebtheme_googlemap();
	}
	$script_out .= '});'."\n";
	$script_out .= "\n".'</script>'."\n\n\n";
	echo $script_out;
}

function iwebtheme_googlemap(){
	$address = iwebtheme_smof_data('map_address');
	$script = '';
	$script .= "jQuery('#googlemaps').gMap({"."\n";
	$script .= "maptype: 'ROADMAP',"."\n";
    $script .= 'scrollwheel: false,'."\n";
    $script .= "zoom: 16,"."\n";
    $script .= 'markers: ['."\n\n";
	$script .= "{"."\n";	
	$script .= "address: '".$address."',"."\n\n";
    $script .= "html: '',"."\n";
    $script .= "popup: false,"."\n";        		
    $script .= "}"."\n";
    $script .= "],"."\n";
	$script .= "});"."\n";
	return $script;
}
function iwebtheme_mailchimp(){
	$script = '';
	$script .= "jQuery('#mailchimp').submit(function() {"."\n";
	$script .= "jQuery('input#email').attr('placeholder','Adding email address...');"."\n";
    $script .= 'jQuery.ajax({'."\n";

    $script .= "type: 'POST',"."\n";
    $script .= 'data: {'."\n\n";
	$script .= "action:'mailchimp_add',"."\n";	
	$script .= 'email: jQuery("#email").val()'."\n\n";
    $script .= "},"."\n";
    $script .= "url: '".get_site_url()."/wp-admin/admin-ajax.php',"."\n";        		
    $script .= "success: function(msg) {"."\n";
    $script .= "jQuery('.response').html(msg);"."\n";
	$script .= "jQuery('input#email').attr('placeholder','Your email here...');"."\n";
    $script .= '},'."\n";
	
	$script .= 'error: function() {'."\n";
	$script .= "jQuery('.response').html('An error occurred');"."\n";
    $script .='} });'."\n\n";
	$script .= 'return false; });'."\n";
	return $script;
}

function iwebtheme_back_top(){

	$script = '';
	$script .= '$("#back-top").hide();'."\n";
    $script .= '$(function () {'."\n";

    $script .= '$(window).scroll(function () {'."\n";

    $script .= 'if ($(this).scrollTop() > 100) {'."\n\n";

	$script .= "$('#back-top').fadeIn();"."\n";
	$script .= '} else {'."\n\n";

    $script .= "$('#back-top').fadeOut(); }});"."\n";

     $script .= "$('#back-top a').click(function () {"."\n";
        		
    $script .= "$('body,html').animate({"."\n";
    $script .= 'scrollTop: 0'."\n";
    $script .= '}, 600); return false; });'."\n";
    $script .='});'."\n\n";


	return $script;
}



/*============================================
Register menu navigation
=============================================*/
register_nav_menus(
	array(
	'main'=>__('Main Menu'),
	'bottom'=>__('Footer Menu'),
	)
);




/*============================================
Theme widgets
=============================================*/
$widgets = array(
				'includes/widget-contact-info.php',
				'includes/widget-flickr.php',
				'includes/widget-social.php',
				'includes/widget-default.php',
				);

// Allow child themes/plugins to add widgets to be loaded.
$widgets = apply_filters( 'of_widgets', $widgets );
				
foreach ( $widgets as $w ) {
	locate_template( $w, true );
}
	


/*============================================
Register sidebars
=============================================*/

register_sidebar(array(
		'name' => 'Primary Sidebar',
		'id'   => 'primary-sidebar',
		'description'   => 'These are widgets for primary sidebar.',
		'before_widget' => '<div class="sidebar-item m-bot-35 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="caption-container-main m-bot-30"><div class="caption-text-container">',
		'after_title'   => '</div><div class="content-container-white caption-bg clearfix"></div></div>'
));	

if ( function_exists('register_sidebar'))
	register_sidebar(array(
		'name' => 'Portfolio Detail Sidebar',
		'id' => 'portfolio-detail-sidebar',
		'description' => 'Widgets in this area will be shown in portfolio detail sidebar',
		'before_widget' => '<div class="sidebar-item m-bot-35 %2$s">',
		'after_widget' => '</div>',
		'before_title'  => '<div class="caption-container-main m-bot-30"><div class="caption-text-container">',
		'after_title'   => '</div><div class="content-container-white caption-bg "></div></div>'
));

if ( function_exists('register_sidebar'))
	register_sidebar(array(
		'name' => 'Footer Column 1',
		'id' => 'footer-1',
		'description' => 'Widgets in this area will be shown in the top footer column 1.',
		'before_widget' => '<div class="footer_widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="caption footer-block">',
		'after_title' => '</h3>',
));
if ( function_exists('register_sidebar'))
	register_sidebar(array(
		'name' => 'Footer Column 2',
		'id' => 'footer-2',
		'description' => 'Widgets in this area will be shown in the top footer column 2.',
		'before_widget' => '<div class="footer_widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="caption footer-block">',
		'after_title' => '</h3>',
));
if ( function_exists('register_sidebar'))
	register_sidebar(array(
		'name' => 'Footer Column 3',
		'id' => 'footer-3',
		'description' => 'Widgets in this area will be shown in the top footer column 3.',
		'before_widget' => '<div class="footer_widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="caption footer-block">',
		'after_title' => '</h3>',
));
if ( function_exists('register_sidebar'))
	register_sidebar(array(
		'name' => 'Small Header Menu',
		'id' => 'small-header-Menu',
		'description' => 'Widgets in this area will be shown above the main nav.',
		'before_widget' => '<div class="small-header-menu %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
));




// TGM plugin
require_once('functions/class-tgm-plugin-activation.php');



function iwebtheme_theme_register_required_plugins() {
	$plugins = array(

			array(
				'name'     				=> 'CF-Post-Formats', // The plugin name
				'slug'     				=> 'cf-post-formats', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/functions/plugins/cf-post-formats.zip', // The plugin source
				'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			array(
				'name'     				=> 'iWebtheme Solana Shortcodes', // The plugin name
				'slug'     				=> 'iwebtheme-solana-shortcodes', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/functions/plugins/iwebtheme-solana-shortcodes.zip', // The plugin source
				'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			array(
				'name'     				=> 'iWebtheme Solana Meta Box', // The plugin name
				'slug'     				=> 'iwebtheme-solana-meta-box', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/functions/plugins/iwebtheme-solana-meta-box.zip', // The plugin source
				'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			array(
				'name'     				=> 'Font Awesome More Icons', // The plugin name
				'slug'     				=> 'font-awesome-more-icons', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/functions/plugins/font-awesome-more-icons.zip', // The plugin source
				'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			array(
				'name'     				=> 'Previous and Next Post in Same Taxonomy', // The plugin name
				'slug'     				=> 'previous-and-next-post-in-same-taxonomy', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/functions/plugins/previous-and-next-post-in-same-taxonomy.zip', // The plugin source
				'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
	);

	// Change this to your theme text domain, used for internationalising strings
	$theme_text_domain = 'iwebtheme';

	$config = array(
		'domain'       		=> $theme_text_domain,         	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_menu_slug' 	=> 'themes.php', 				// Default parent menu slug
		'parent_url_slug' 	=> 'themes.php', 				// Default parent URL slug
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
		'message' 			=> '',							// Message to output right before the plugins table
			'strings'      		=> array(
				'page_title'                       			=> __( 'Install Required Plugins', $theme_text_domain ),
				'menu_title'                       			=> __( 'Install Plugins', $theme_text_domain ),
				'installing'                       			=> __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
				'oops'                             			=> __( 'Something went wrong with the plugin API.', $theme_text_domain ),
				'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
				'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
				'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
				'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
				'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
				'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
				'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
				'return'                           			=> __( 'Return to Required Plugins Installer', $theme_text_domain ),
				'plugin_activated'                 			=> __( 'Plugin activated successfully.', $theme_text_domain ),
				'complete' 									=> __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
				'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
			)
	);

	tgmpa( $plugins, $config );

}
?>