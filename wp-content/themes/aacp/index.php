<?php
/**
 * @package WordPress
 */
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
?>
<?php get_header(); ?>
<!-- PAGE TITLE -->
	<div class="container m-bot-35 clearfix">
		<div class="sixteen columns">
			<div class="page-title-container clearfix">
				<h1 class="page-title"><?php echo __('LATEST POSTS','iwebtheme'); ?></h1>
			</div>	
		</div>
	</div>	
	
</div>	<!-- Grey bg end -->
<div class="container clearfix">

	<?php if ($sidebar_pos == 'left') { ?>
		<?php get_sidebar(); ?>
	<?php } ?>

	<div class="eleven columns m-bot-25">
		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'includes/content', get_post_format() ); ?>
			<?php endwhile; ?>
			<?php if (function_exists("pagination")) { ?>
			<div class="pagination-1-container">
			<?php pagination(); ?>
			</div>
			<?php } else {
			posts_nav_link(' &#183; ', 'previous page', 'next page'); 	
			} ?>
		<?php endif; // end have_posts() check ?>

	</div>

	<?php if ($sidebar_pos == 'right') { ?>
		<?php get_sidebar(); ?>
	<?php } ?>	

</div>
<!--end content-->
<?php get_footer(); ?>