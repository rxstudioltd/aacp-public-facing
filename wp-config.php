<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

$domain = $_SERVER['SERVER_NAME'];

if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
  include( dirname( __FILE__ ) . '/wp-config-local.php' );
  
// Otherwise use the below settings (on live server)
} else {
 
  // Live Server Database Settings
  define( 'DB_NAME',     'aacplive');
  define( 'DB_USER',     'aacplive');
  define( 'DB_PASSWORD', 'aacpsteveis12' );
  define( 'DB_HOST',     'localhost'  );
  
  // Overwrites the database to save keep edeting the DB
  define('WP_HOME','http://www.aacp.co.uk');
  define('WP_SITEURL','http://www.aacp.co.uk');
  
  // Turn Debug off on live server
  define('WP_DEBUG', false);
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HAr%p].cw53UFxo5qJM? !W@%vXSsNqk(21B(^g$7Qeje3b:>uh<x4+:+o:ZyyNl');
define('SECURE_AUTH_KEY',  'C2q-<y*<HUqF=e.|lNA2hm/nhVQH[Bq6VsYb1(3ER1f} $:onv}%-8/^iP:j7i3b');
define('LOGGED_IN_KEY',    'V3Nj5,&V`o8u[!?FbYTX|(YM~Jv|F{g*67go{%--|Ixh1&>_h%I?S~[id+;Z&r k');
define('NONCE_KEY',        'd++*u~O~]>Ec.lY;H4J`zS%C#tLhn>1F1Y>[>4o_>5)N3/uOdk!^OoXc(qwTz/c=');
define('AUTH_SALT',        'SEgj/F~}]uMzQ6aqjnFRzlwJr>%]5&}d,sF LYNNI#/-liJ}8<E>tGNE(<ID/0v&');
define('SECURE_AUTH_SALT', 'lVT-*4wmWJmQz%WfMgI;@a]090gMEu-S6SJ[`awVaAPuFl}u$l$K>cNfLf+{FPt(');
define('LOGGED_IN_SALT',   '-WG~iXh:>op;AiL_!`s*1%/I5(q_@h(}5*Ul1FVa)rv/3l5A:+!Ha%qF@_W`aB?Y');
define('NONCE_SALT',       'VI3/_&)lV|ZaTZH_:+Pua$S=3NQG1SHpShrpb+D>fxT7#r.G^j+761j6sn|,I|_+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
